{
	inputs = {
		nixpkgs.url = "nixpkgs/nixos-23.11";
		home-manager.url = github:nix-community/home-manager/release-23.11;
		home-manager.inputs.nixpkgs.follows = "nixpkgs";
	};

	outputs = { nixpkgs, home-manager, ... }:
	let
		username = "efpalaciosmo";
		email = "efpalaciosmo@unal.edu.co";
		locale = "en_US.UTF-8";
		timeZone = "America/Bogota";
		system = "x86_64-linux";
	in {
		homeConfigurations.${username} = home-manager.lib.homeManagerConfiguration {
			extraSpecialArgs = {
				inherit username;
				inherit email;
				inherit locale;
				inherit timeZone;
				inherit system;
			};
			pkgs = import nixpkgs { inherit system; config = { allowUnfree = true;}; };
			modules = [ ./home.nix ];
		};
		xdg = {
			userDirs = {
				enable = true;
				createDirectories = true;
			};
			portal = {
				enable = true;
				extraPortal = with nixpkgs; 
					[
						xdg-desktop-portal-hyprland
						xdg-desktop-portal-gtk
					];
			};
		};
	};

}
