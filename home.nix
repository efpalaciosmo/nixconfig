{ config, pkgs, username, locale, timeZone, ... }:

{

	# imports
	imports = 
		[
			./packages/packages.nix
		];
	home.username = "${username}";
	home.homeDirectory = "/home/${username}";

	# Home manger version
	home.stateVersion = "23.11"; # Please read the comment before changing.

	# Manage dotfiles
		home.file = {
		};
	# Let Home Manager install and manage itself.
	programs.home-manager.enable = true;
}
