{ libs, config, pkgs, ... }:
{
	imports = [
		./obs.nix
		./games.nix
	];

	home.packages = with pkgs; [
		firefox
		xfce.thunar
		xfce.thunar-volman
		xfce.thunar-archive-plugin
		xfce.thunar-media-tags-plugin
		armcord
		cider
		telegram-desktop
		thunderbird
		cemu
		dolphin-emu
		pcsx2
		brave
		microsoft-edge
	];
}
