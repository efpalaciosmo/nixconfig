{ libs, config, pkgs, ... }:

{
	imports = 
		[
			./ides.nix
		]
	home.packages = with pkgs; 
	[
		nodePackages.typescript-language-server
		gcc13
	];
}
