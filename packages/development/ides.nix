{ libs, config, pkgs, ...}:

{
	programs.vscode = {
		enable = true;
		package = pkgs.vscodium;
		#extensions = with pkgs.vscode-extensions; 
		#[
		#	piousdeer.adwaita-theme
		#];
	# Enable vsmarketplace
}
