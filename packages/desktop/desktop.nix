{ libs, config, pkgs, username, ... }: 
{
	#gtk = {
	#	enable = true;
	#	#gtk3.extraConfig = {
 	#	#	Settings = ''
	#	#	gtk-application-prefer-dark-theme=1
	#	#	'';
	#	#};
	#	#gtk4.extraConfig = {
 	#	#	Settings = ''
	#	#	gtk-application-prefer-dark-theme=1
	#	#	'';
	#	#};
	#};
	qt = {
		enable = true;
		platformTheme = "gtk";
		style = {
			name = "adwaita-dark";
			package = pkgs.adwaita-qt6;
		};
	};

	home.pointerCursor = {
		gtk.enable = true;
		x11.enable = true;
		package = pkgs.bibata-cursors;
		name = "Bibata-Modern-Ice";
		size = 25;
	};
	programs.rofi.enable = true;
	programs.waybar.enable = true;
	wayland.windowManager.hyprland.enable = true;
	wayland.windowManager.hyprland.extraConfig = "$terminal = alacritty\n$menu = rofi --show drun";

}
