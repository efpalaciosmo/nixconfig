{ libs, config, pkgs, username, ... }:
{
	imports =
	[	
		./bash.nix
		./git.nix
		./tools.nix
		./alacritty.nix
		./fonts.nix
	];
	home.sessionVariables = {
		EDITOR = "nvim";
	};

	home.sessionPath = [ "$HOME/.local/bin" ];
}
