{ libs, config, pkgs, ... }:

{
	programs.zsh = {
		enable = true;
		enableCompletion = true;
		enableAutosuggestions = true;
		dotDir = ".config/zsh";
	};
	programs.starship = {
		enable = true;
	};
}
