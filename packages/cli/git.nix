{ libs, pkgs, config, username, email, ...}:

{
	programs.git = {
		enable = true;
		userEmail = "${email}";
		userName = "${username}";
	};
}
		
