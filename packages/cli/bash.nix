{ libs, config, pkgs, ... }:

{
	programs.bash = {
		enable = true;
		enableCompletion = true;
		enableVteIntegration = true;
		shellAliases = {
			".."="cd ..";
		};
		historyIgnore = [
			"ls"
			"cd"
			"cd .."
			".."
			"exit"
			"htop"
			"neofetch"
			"vim"
			"vi"
			"nvim"
		];
	};
}
