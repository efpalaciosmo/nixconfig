{ lib, config, pkgs, ...}:
{
	home.packages = with pkgs; [
		btop
		ripgrep
		mpv
		tre
		wget
		curl
		neofetch
		gvfs
		tldr
		duf
		bat
		dunst
		zathura
		slurp
		grim
		neovim
	];
}
