{libs, config, pkgs, ... }:
{
    imports = 
    [
		./cli/cli.nix
		./applications/applications.nix
		./desktop/desktop.nix
    ];
	home.packages = with pkgs; [
		pkgs.xorg.xf86videoqxl
	];
}   
